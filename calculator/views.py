from django.shortcuts import render
from django.http import HttpResponse
from django.template import loader

# Create your views here.

def calculator(request):
	template = 'calculator/calculator.html'
	return render(request, template)

def result(request):
	template = 'calculator/result.html'
	value1 = int(request.GET['value1'])
	value2 = int(request.GET['value2'])
	ans = None
	if request.GET['operator'] == '+':
		ans = value1 + value2
	elif request.GET['operator'] == '-':
		ans = value1 - value2
	elif request.GET['operator'] == '*':
		ans = value1 * value2
	elif request.GET['operator'] == '/':
		ans = value1 / value2
	return render(request, template , {'answer':ans})
